package com.lead.consult.base;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestBase {
	protected WebDriver driver;
	protected WebDriverWait wait;
	//region Methods
	@Before
	public void setUp() {

		String chromeDriverPath = "D:\\Tisho\\Automation\\Java\\chromedriver_win32\\chromedriver.exe";

		System.setProperty("webdriver.chrome.driver", chromeDriverPath);

		driver = new ChromeDriver();

		wait = new WebDriverWait(driver, 10);
	}

	@After
	public void tearDown() {

		driver.quit();
	}
	//endregion
}
