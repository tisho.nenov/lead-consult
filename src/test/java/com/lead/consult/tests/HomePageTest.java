package com.lead.consult.tests;

import org.junit.Assert;
import org.junit.Test;

import com.lead.consult.base.TestBase;

import pages.BusinessPartnersPage;
import pages.HomePage;
import pages.ProductsPage;
import pages.ServicesPage;

public class HomePageTest extends TestBase {

	@Test
	public void testHomePageTitle() {
		// Arrange: Create page objects.
		HomePage homePage = new HomePage(driver);
		ServicesPage servicesPage = new ServicesPage(driver);
		ProductsPage productsPage = new ProductsPage(driver);
		BusinessPartnersPage businessPartnersPage = new BusinessPartnersPage(driver);

		// Arrange: Open Home Page.
		homePage.navigateToHomePage();
		Assert.assertTrue("Error: Home Page is not opened.", homePage.isHomePageOpened());

		// Act: Open Services menu and verify Services page.
		homePage.openMenuServices();
		Assert.assertTrue("Error: Services Page is not opened.", servicesPage.isOurServicesTitleVisible());

		// Navigate back to Home Page.
		homePage.navigateToHomePage();

		// Act: Open Products menu and verify Products page.
		homePage.openMenuProducts();
		Assert.assertTrue("Error: Products Page is not opened.", productsPage.isOurProductsTitleVisible());

		// Navigate back to Home Page.
		homePage.navigateToHomePage();

		// Act: Open Our Partners menu, select Business Partners submenu, and verify Business Partners page.
		homePage.openMenuOurPartner();
		homePage.selectSubMenuBusinessPartners();
		Assert.assertTrue("Error: Business Partners Page is not opened.",
				businessPartnersPage.isBusinessPartnersTitleVisible());
	}
}
