package com.lead.consult.tests;

import org.junit.Assert;
import org.junit.Test;

import com.lead.consult.base.TestBase;

import pages.ContactUsPage;
import pages.HomePage;

public class ContactUsPageTest extends TestBase {

	@Test
	public void testContactUsPage() {
		// Arrange: Create page objects.
		HomePage homePage = new HomePage(driver);
		ContactUsPage contactUsPage = new ContactUsPage(driver);

		// Arrange: Open Home Page.
		homePage.navigateToHomePage();

		// Act: Open Contact Us Page and verify.
		homePage.openContactUsPage();
		Assert.assertTrue("Error: Contact Us Page is not opened.", contactUsPage.isContactUsPageOpened());

		// Act: Fill Contact Us form.
		contactUsPage.fillContactForm();

		// Act: Send Contact Us form.
		contactUsPage.agreeProcessPersData();
		contactUsPage.sendForm();

		Assert.assertTrue("Error: Captcha error message not shown", contactUsPage.isCaptchaErrorMsgShow());
	}
}
