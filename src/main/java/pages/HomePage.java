package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
	public WebDriver driver;
	private WebDriverWait wait;

	// region Elements Locators
	@FindBy(id = "top")
	private WebElement topSection;

	@FindBy(id = "menu-item-6363")
	private WebElement servicesMenu;

	@FindBy(id = "menu-item-7437")
	private WebElement productsMenu;

	@FindBy(id = "menu-item-8088")
	private WebElement ourPartnerMenu;

	@FindBy(id = "menu-item-8087")
	private WebElement businessPartnersSubMenu;

	@FindBy(id = "menu-item-5819")
	private WebElement contactUsMenu;
	// endregion

	// region Methods
	public HomePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 10);

		PageFactory.initElements(driver, this);
	}

	/**
	 * Navigates to the Home Page.
	 */
	public void navigateToHomePage() {
		driver.get("https://www.leadconsult.eu/");
	}

	/**
	 * Checks if the Home Page is opened.
	 *
	 * @return True if the Home Page is opened, False otherwise.
	 */
	public boolean isHomePageOpened() {
		wait.until(ExpectedConditions.visibilityOf(topSection));
		return topSection.isDisplayed();
	}

	/**
	 * Opens the Services menu.
	 */
	public void openMenuServices() {
		servicesMenu.click();
	}

	/**
	 * Opens the Products menu.
	 */
	public void openMenuProducts() {
		productsMenu.click();
	}

	/**
	 * Opens the Our Partner menu.
	 */
	public void openMenuOurPartner() {
		ourPartnerMenu.click();
	}

	/**
	 * Selects the Business Partners submenu.
	 */
	public void selectSubMenuBusinessPartners() {
		businessPartnersSubMenu.click();
	}

	/**
	 * Opens the Contact Us page.
	 */
	public void openContactUsPage() {
		contactUsMenu.click();
	}
	// endregion
}
