package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductsPage {
	private WebDriverWait wait;

	// region Elements Locators
	@FindBy(xpath = "//*[@id=\"post-7393\"]/div/section[1]/div[2]/div/div/div/div/div/h1")
	private WebElement titleOurProducts;
	// endregion

	// region Methods
	public ProductsPage(WebDriver driver) {
		wait = new WebDriverWait(driver, 10);
		PageFactory.initElements(driver, this);
	}

	/**
	 * Checks if the Our Products title is visible.
	 *
	 * @return True if the Our Products title is visible, False otherwise.
	 */
	public boolean isOurProductsTitleVisible() {
		wait.until(ExpectedConditions.visibilityOf(titleOurProducts));
		return titleOurProducts.isDisplayed();
	}

	// endregion
}
