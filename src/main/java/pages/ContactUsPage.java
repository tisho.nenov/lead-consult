package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.github.javafaker.Faker;

public class ContactUsPage {
	public WebDriver driver;
	private WebDriverWait wait;

	// region Elements Locators
	@FindBy(xpath = "//*[@id=\"contact\"]/div[2]/div/div[1]/div/div/div[1]/h2")
	private WebElement title;

	@FindBy(css = "#wpcf7-f5661-p6126-o1>form>p:nth-child(2)>label>span>input")
	private WebElement yourName;

	@FindBy(css = "#wpcf7-f5661-p6126-o1>form>p:nth-child(3)>label>span>input")
	private WebElement yourEmail;

	@FindBy(css = "#wpcf7-f5661-p6126-o1>form>p:nth-child(4)>label>span>input")
	private WebElement phoneNumber;

	@FindBy(css = "#wpcf7-f5661-p6126-o1>form>p:nth-child(5)>label>span>textarea")
	private WebElement yourMessage;

	@FindBy(xpath = "//*[@id=\"wpcf7-f5661-p6126-o1\"]/form/p[6]/span/span/span/label/input")
	private WebElement checkBox;

	@FindBy(xpath = "//*[@id=\"wpcf7-f5661-p6126-o1\"]/form/p[7]/input")
	private WebElement sendBtn;

	@FindBy(xpath = "//*[@id=\"wpcf7-f5661-p6126-o1\"]/form/span/span[2]")
	private WebElement captchaMsg;
	// endregion

	// region Methods
	public ContactUsPage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 10);

		PageFactory.initElements(driver, this);
	}

	/**
	 * Checks if the Contact Us page is opened.
	 * 
	 * @return true if the page is opened, false otherwise
	 */
	public boolean isContactUsPageOpened() {
		wait.until(ExpectedConditions.visibilityOf(title));
		return title.isDisplayed();
	}

	/**
	 * Checks if the captcha error message is shown.
	 * 
	 * @return true if the captcha error message is shown, false otherwise
	 */
	public boolean isCaptchaErrorMsgShow() {
		wait.until(ExpectedConditions.visibilityOf(captchaMsg));
		return captchaMsg.isDisplayed();
	}

	/**
	 * Fills the contact form with random values from.
	 */
	public void fillContactForm() {
		Faker faker = new Faker();
		yourName.sendKeys(faker.name().fullName());
		yourEmail.sendKeys(faker.internet().emailAddress());
		phoneNumber.sendKeys(faker.phoneNumber().phoneNumber());
		yourMessage.sendKeys(faker.lorem().sentence());

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Checks the checkbox to agree with processing personal data.
	 */
	public void agreeProcessPersData() {
		checkBox.click();
	}

	/**
	 * Sends the contact form.
	 */
	public void sendForm() {
		sendBtn.click();
	}

	// endregion
}
