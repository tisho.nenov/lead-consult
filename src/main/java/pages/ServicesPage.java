package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ServicesPage {
	private WebDriverWait wait;

	// region Elements Locators
	@FindBy(xpath = "//*[@id=\"post-6321\"]/div/section[1]/div[2]/div/div/div/div/div/h1")
	private WebElement titleOurServices;
	// endregion

	// region Methods
	public ServicesPage(WebDriver driver) {
		wait = new WebDriverWait(driver, 10);
		PageFactory.initElements(driver, this);
	}

	/**
	 * Checks if the Our Services title is visible.
	 *
	 * @return True if the Our Services title is visible, False otherwise.
	 */
	public boolean isOurServicesTitleVisible() {
		wait.until(ExpectedConditions.visibilityOf(titleOurServices));
		return titleOurServices.isDisplayed();
	}
	// endregion
}
