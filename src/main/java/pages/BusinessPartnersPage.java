package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BusinessPartnersPage {
	private WebDriverWait wait;

	// region Elements Locators
	@FindBy(xpath = "//*[@id=\"business\"]/div[2]/div/div/div/div/div/h1")
	private WebElement titleBusinessPartners;
	// endregion

	// region Methods
	public BusinessPartnersPage(WebDriver driver) {
		wait = new WebDriverWait(driver, 10);

		PageFactory.initElements(driver, this);
	}

	/**
	 * Checks if the Business Partners title is visible.
	 *
	 * @return True if the Business Partners title is visible, False otherwise.
	 */
	public boolean isBusinessPartnersTitleVisible() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		wait.until(ExpectedConditions.visibilityOf(titleBusinessPartners));
		return titleBusinessPartners.isDisplayed();
	}
	// endregion
}
